# NSI en terminal

L'ensemble du programme est disponible sur la page d'accueil des [terminals](/terminal.md)

## Pour réviser 

- Des épreuves pratiques de spécialité sur [eduscol](https://eduscol.education.fr/2661/banque-des-epreuves-pratiques-de-specialite-nsi),
- Des notebooks pour réviser sur [isn-icn-ljm](https://isn-icn-ljm.pagesperso-orange.fr/revisions/co/section_notebook.html),
- Des exercices d'épreuves pratiques et leur corrigés sur [isn-icn-ljm](https://isn-icn-ljm.pagesperso-orange.fr/revisions/co/epreuve_pratique.html)
