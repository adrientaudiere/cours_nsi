# Hygiène numérique

Si cela vous intéresse vous trouverez un grand nombre de ressources en français sur le site [GoFoss](https://gofoss.net/fr/) (littéralement "En avant pour les logiciels libres").

Pour ceux qui sont à l'aise avec l'anglais je vous conseille fortement le [Guide de la vie privé](https://www.privacyguides.org/tools/) qui est très complet et un bon compromis entre technicité et pédagogie.

## Mot de passe

[![xkcd](https://imgs.xkcd.com/comics/password_strength.png)](https://xkcd.com/936/)

---

<p class="center-p">

**Choix du mot de passe** par [xkcd](https://xkcd.com): Licence [CC-BY-SA 2.5](https://creativecommons.org/licenses/by-sa/2.5/)

</p>

Il est fortement conseillé d'utiliser un gestionnaire de mot de passe. L'idée est de vous souvenir d'un seul mot de passe maître (du coup on peut le choisir long) qui permet d'accéder à tous ces mots de passe que l'on a alors pas besoin de retenir.

!> Il faut se souvenir de son mot de passe maître!

!> Il faut utiliser un gestionnaire de mot de passe sûr.

## Écologie et numérique

<https://httparchive.org/reports/page-weight>

## La neutralité du net

[#Datageule: Neutralité, j'écris ton nom](https://framatube.org/w/64077068-5d05-4815-9095-af63a33a91c4)
Site de l'association [La Quadrature du Net](https://www.laquadrature.net/neutralite_du_net/)

## Apprendre à écrire au clavier

Pour écrire plus rapidement et plus efficacement, il est important de s'entraîner. Il existe plusieurs logiciels pour s'entraîner: [Ktouch](https://apps.kde.org/fr/ktouch/) est vraiment très agréable et prenant. [Klavaro](https://klavaro.sourceforge.io/fr/index.html) est une bonne alternative sous Windows, Mac et Linux. [Monkeytype](https://monkeytype.com/) permet ensuite de tester sa rapidité en ligne.

## Aller plus loin dans l'apprentissage de l'informatique

### Devenir développeur

De nombreuses formations sont disponibles en lignes. Le plus difficile est d'identifier ce qu'il faut apprendre. Pour cela vous pouvez aller voir les guides visuels de conseils d'apprentissages sur [roadmap.sh](https://roadmap.sh)

- [développeur frontend](https://roadmap.sh/frontend)
- [développeur backend](https://roadmap.sh/backend)
- [développeur python](https://roadmap.sh/python)
- [développeur android](https://roadmap.sh/android)

### Ressources pour python

- [liste "awesome python" *(en)*](https://github.com/vinta/awesome-python)
- [liste d'algorithmes avec python *(en)*](https://github.com/TheAlgorithms/Python)
- [Invent with python *(en)*](https://inventwithpython.com/)


### Ressources Markdown

- [Tutoriel Markdown](https://www.markdowntutorial.com/fr/)