# Sources

## Sources principales

- Les cours de Frédéric Mandon ([http://www.maths-info-lycee.fr/](http://www.maths-info-lycee.fr/)) ont servis de fondation très utile pour construire ce cours. Les cours de F. Mandon ont utilisés comme sources importantes d'inspiration le livre de G.Swinnen : Apprendre à programmer avec
  Python 3

- Les cours de Pierre-Erwan Le Marec et Jean Mortreux, enseignant de NSI à Scholae 2019-2020 pour les premières. Ces cours m'ont en particulier aidé à structurer les parties « Histoire de l'informatique » et « codage-typage ».

- Les cours de Stéphan Van Zuijlen ([https://isn-icn-ljm.pagesperso-orange.fr](https://isn-icn-ljm.pagesperso-orange.fr/)) sous licence libre.

- Les cours en ligne de [Mon lycée numérique](http://www.monlyceenumerique.fr/index_nsi.html).

- Les cours en ligne de Pascal Grossé au [lycée Carnot](https://python-carnot.fr/) sous licence libre.

- Le livre Numérique et Sciences de l'Informatiques pour les premières (éd. Ellipses) co-écrit par T. Balabonski, S. Conchon, J.-C.
  Filliâtre et K. Nguyen.

- Les cours en ligne du site [librecours.net](https://librecours.net) écrit par [Stéphane Crozat](http://stph.crzt.fr) dont la majorité est sous licence [CC-by-sa-3.0](http://creativecommons.org/licenses/by-sa/3.0/fr/).

- Le travail des lycéens de Scholae pour améliorer le site.

- [eduscol](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g)

## Sources secondaire

<https://www.apprendre-en-ligne.net/index-info.html>

<http://jargonf.org/wiki/Accueil>

### Se former
#### Se former en ligne


[France IOI](http://www.france-ioi.org/algo/chapters.php) `python`

[Open Class Room](https://openclassrooms.com/fr/) 

[Freecodecamp](<https://freecodecamp.org/>) `js` `html` `css`

[Libre Cours](<https://librecours.net/>)

[kanacademy](https://www.khanacademy.org/computing/computer-programming/html-css) `html` `css`

[htmldog](https://htmldog.com/guides/) `html` 

#### Se former en jouant

[hackinscience](https://www.hackinscience.org/exercises/)

[py-rates](https://py-rates.org/index.html)

[Oh my git](https://ohmygit.org/)

#### Se former sans écran (activités débranchées)

- [castor-informatique](https://castor-informatique.fr/)
- [Médiation ](https://members.loria.fr/MDuflot/files/mediation.html)
- [Un livre sur l'apprentissage de l'informatique débranché](https://interstices.info/wp-content/uploads/2018/01/csunplugged2014-fr-comp.pdf)

###  Internet

- [L'internet rapide et permanent](http://irp.nain-t.net/doku.php/start) (CC BY-NC-SA 2.0 FR)
- Routage sur [idum](http://idum.fr/spip.php?article213)

### Histoire de l'informatique

<https://pablo.rauzy.name/teaching/hi/>


### Outils et langages préférés des développeurs

- [Survey stackoverflow](https://insights.stackoverflow.com/survey/)
- [Tiobe index](https://www.tiobe.com/tiobe-index/)
- [pypl](https://pypl.github.io/PYPL.html)
- [Code.gouv](https://code.gouv.fr/#/stats)
- [state of JS](https://2021.stateofjs.com/fr-FR/)

### À creuser

<https://science.sciencemag.org/content/sci/332/6025/60/F2.large.jpg>

<https://royalsocietypublishing.org/doi/10.1098/rsos.181393>

<https://www.martinhilbert.net/worldinfocapacity-html/>

<http://documents1.worldbank.org/curated/en/896971468194972881/310436360_201602630200201/additional/102725-PUB-Replacement-PUBLIC.pdf>

<https://townline.org/welcome-to-the-world-of-big-data/>

<https://www.internetlivestats.com/>

<https://lyc-84-bollene.gitlab.io/chambon/>

### Chaîne youtube NSI

#### Terminal 

https://www.youtube.com/playlist?list=PLmu0dyrWo2AR5SABHluBseMDxPpq5OEmb
https://www.youtube.com/watch?v=XbXEpBe6EGg&list=PL2V_2xzaQW7dz4-gBt4Hj3VrmTskn4l2N

#### Première

https://www.youtube.com/watch?v=EHuwB082jEE&list=PL2V_2xzaQW7fUYnjbOZdj0_It7BeRXBQx_de